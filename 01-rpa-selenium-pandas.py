from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains
import pandas as panda
import time

PATH_DRIVER = "D:/python-ejemplos/chrome92045driver.exe"

chromedriver = webdriver.Chrome(PATH_DRIVER)

linio = "https://www.linio.com.co/cm/coches-de-bebe"
linkBaseToProduct = "https://www.linio.com.co"

chromedriver.get(linio)
time.sleep(5)

pageContent = BeautifulSoup(chromedriver.page_source,"html.parser")

articulo_title = []
articulo_price = []
articulo_link = []

total_page_to_view = 5

for page in range(0, total_page_to_view):
    productos = pageContent.find_all("div", attrs={ 'class' : 'catalogue-product row','itemtype': 'http://schema.org/Product'})
    for articulo in productos:
        articulo_title.append(articulo.find_next("span", attrs={ 'class' : "title-section"}).text)
        articulo_price.append(articulo.find_next("span", attrs={ 'class' : "original-price"}).text)
        articulo_link.append(linkBaseToProduct+articulo.find_next("a", attrs={ 'class' : "rating-container" }).attrs['href'])

    nextPage = chromedriver.find_element_by_xpath("/html/body/div[3]/main/div/div[10]/div[2]/nav/ul/li[6]/a")
    ActionChains(chromedriver).click(nextPage).perform()
    time.sleep(6)
    
panda_articulos = panda.DataFrame({
                                'TITULO': articulo_title,
                                'PRECIO': articulo_price,
                                'LINK': articulo_link
                            })

#print(panda_articulos)

panda_articulos.to_csv(r"D:/python-ejemplos/web-scrapping/linio/linio.csv",index=None, header=True)

chromedriver.quit()

